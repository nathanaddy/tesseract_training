# Overview

This software provides a Virtual Machine with two sets of scripts
demonstrating basic building of Tesseract languages and adding fonts
to languages.

Currently scripts are very much in progress. See sections 'Things to
Do' for some things worth looking into.

# Installation

The only installable thing is a Vagrant virtual machine. If you have
vagrant installed ```vagrant up --provider virtualbox``` should be
sufficient to install the machine for the first time.

## Vagrant

Download: https://www.vagrantup.com/downloads.html
Documentation: https://www.vagrantup.com/docs/

Start the vagrant machine the first time by running

   ```$ vagrant up --provider virtualbox```

This will download the Debian Stretch image for the virtual machine as
described in the Vagrantfile. The flag '--provider virtualbox' only has to
be done to download the image the very first time, which is saved as
an available virtual machine for future use across different virtual
machines.

Generally, the way vagrant is used is (running from the directory the
Vagrantfile is located in):

```
$ vagrant     # Show possible vagrant commands/help.
$ vagrant up  # This starts the virtual machine if it is halted.
$ vagrant ssh # ssh into the virtual machine
```

### Provisioning

The first time 'vagrant up' is run the Debian image is provisioned,
installing Tesseract and the Tesseract training tools. This only needs
to be done once; however, if the provisioning gets updated (see the
last lines of the Vagrantfile), then provisioning should be rerun
using ```vagrant reload --provision```.

## Logging into the virtual machine

Log into the virtual machine by running ```vagrant ssh```.  The
training_data directory is mounted at ~/training in the virtual env
(so you can edit the files using the tools on your local machine,
save, and run the scripts in the virtual machine).

## Running Code

Right now there are two files in two directories 'example_training'
and 'papyrus-training' that do something start to
finish. 'google-10000-english' is a list of the common words in the
English language.  'compare_tesseract.py' is a script that at least is
the start

### example_training

Seems to be a variation of the ideas found here (box/tiff strategy)
https://gist.github.com/chroman/5745206


### papyrus_training

Seems to be built around the text2image file, which can generate
tiff/box files from a standard training text.

## Future Work

### Combining the two approaches

example_training appears to be built around pre-made box/tiff files
built by google; I believe all these combined together can produce the
eng language file that should be the same as what's found in the
tesseract-ocr-eng package.  This could/should be verified.

the papyrus example is actually set to produce a language file by
taking the Verdana ttf font file, using text2image to produce tiffs
for training, and training Verdana.  There were issues with extracting
the correct font name from the ttf file.

Presumably some of the steps to prepare a final language file is:

  * figuring out how to combine the optimized box/tiff files with our text2image process
  * optimizing the standard training text for text2image.  Just menu words?
  * optimizing text2image params, add blurryness, rotation, skew, etc
  * correctly defining the right config files for the language (set whitelist, blacklist, etc)
  * looking into the files we aren't putting into the language files and see which are useful

### Files we aren't using

Currently we are only importing a handful of the files that can be
used by tesseract. For instance, I don't believe we're adding dawg
files or anything else.

From the literature I had found online, many are useful - some
probably aren't (some things - I forget what - are only recommended
for glyph-bases languages like Hindi).

```
Combining tessdata files
TessdataManager combined tesseract data files.
Offset for type  0 (eng.config                ) is -1
Offset for type  1 (eng.unicharset            ) is 140
Offset for type  2 (eng.unicharambigs         ) is -1
Offset for type  3 (eng.inttemp               ) is 5799
Offset for type  4 (eng.pffmtable             ) is 682100
Offset for type  5 (eng.normproto             ) is 682893
Offset for type  6 (eng.punc-dawg             ) is -1
Offset for type  7 (eng.word-dawg             ) is -1
Offset for type  8 (eng.number-dawg           ) is -1
Offset for type  9 (eng.freq-dawg             ) is -1
Offset for type 10 (eng.fixed-length-dawgs    ) is -1
Offset for type 11 (eng.cube-unicharset       ) is -1
Offset for type 12 (eng.cube-word-dawg        ) is -1
Offset for type 13 (eng.shapetable            ) is 695934
Offset for type 14 (eng.bigram-dawg           ) is -1
Offset for type 15 (eng.unambig-dawg          ) is -1
Offset for type 16 (eng.params-model          ) is -1
Output eng.traineddata created successfully.
```

Looking at this everything with a -1 is not added.  So config files
for the language (although those can be set/overridden at runtime as
well), various dawgs and cube files.
