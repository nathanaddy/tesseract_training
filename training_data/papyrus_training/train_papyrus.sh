#! /bin/bash

# export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/vagrant/usr/lib
export TESSDATA_PREFIX=/usr/share/tesseract-ocr/
# export TESSBIN=/home/vagrant/usr/bin
export TESSBIN=/usr/bin
export ROOT=`pwd`
export FONTDIR=$ROOT/font
export TESTDIR=$ROOT/testimage
export OUTDIR=$ROOT/output


export FONT=$FONTDIR/Verdana.ttf
export FONTNAME=Verdana
export FONTSTRNAME=Verdana
#FONTSOPTS=--char_spacing=4
export FONTSOPTS=

export LANG=nja

function enter_to_continue {
    read  -p "Enter to continue> " mainmenuinput
    echo ""
}

TRAINING_TEXT=`pwd`/standard-training-text.txt

rm -fr ./tessenv

# Build the environment

mkdir tessenv; cd tessenv
ENVROOT=`pwd`

mkdir $ENVROOT/stockfonts
mkdir $ENVROOT/build
mkdir $ENVROOT/build/$LANG # Change this to extras if I can.

cp $FONT $ENVROOT/stockfonts

cd $ENVROOT/build/$LANG

$TESSBIN/text2image --text=$TRAINING_TEXT --outputbase=$LANG.$FONTNAME.exp0 --font="$FONTSTRNAME" --fonts_dir=$FONTDIR $FONTSOPT
#enter_to_continue

# Create the font_properties file
echo "$FONTSTRNAME 0 0 0 0 0" >> font_properties
#enter_to_continue

# BEGIN BUILDING NEW STUFF
$TESSBIN/tesseract $LANG.$FONTNAME.exp0.tif $LANG.$FONTNAME.exp0 nobatch box.train
#enter_to_continue

$TESSBIN/unicharset_extractor $LANG.$FONTNAME.exp0.box
#enter_to_continue

$TESSBIN/shapeclustering -F font_properties -U unicharset  $LANG.$FONTNAME.exp0.tr
#enter_to_continue

$TESSBIN/mftraining -F font_properties -U unicharset -O $LANG.unicharset $LANG.$FONTNAME.exp0.tr
#enter_to_continue

$TESSBIN/cntraining $LANG.$FONTNAME.exp0.tr
# enter_to_continue

cp inttemp $LANG.inttemp
cp normproto $LANG.normproto
cp pffmtable $LANG.pffmtable
cp shapetable $LANG.shapetable
cp unicharset $LANG.unicharset

$TESSBIN/combine_tessdata $LANG.
# enter_to_continue

echo "Copying File."
sudo cp $LANG.traineddata $TESSDATA_PREFIX/tessdata/$LANG.traineddata
# enter_to_continue

echo "Done building single $LANG file"

#############################

mkdir -p $OUTDIR

echo "Trying to run tesseract."
$TESSBIN/tesseract $LANG.$FONTNAME.exp0.tif basic_output -l $LANG
echo "Read $LANG.$FONTNAME.exp0.tif"
echo "Output head:"
head -20 basic_output.txt
mv basic_output* $OUTDIR
echo "Done"


# echo "Running files in $TESTDIR"
# for file in `ls $TESTDIR`; do
#     $TESSBIN/tesseract $file $file.output -l $LANG
#     mv $file.output* $OUTDIR
# done
