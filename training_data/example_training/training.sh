#! /bin/bash

# build the environment
# export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/home/vagrant/usr/lib
# export TESSDATA_PREFIX=/home/vagrant/usr/share/
# TESSERACT_PATH=/home/vagrant/usr/bin
# PATH=$TESSERACT_PATH:$PATH

ROOT=`pwd`

rm -fr tessenv

mkdir tessenv; cd tessenv
TROOT=`pwd`
mkdir -p $TROOT/stockfonts;
mkdir -p $TROOT/build;
mkdir -p $TROOT/build/eng
echo "Environment built"
# Get the stock english fonts from Google (old, but they work)
cd $TROOT/stockfonts

# Downloaded from http://tesseract-ocr.googlecode.com/files/boxtiff-2.01.eng.tar.gz
cp ${ROOT}/boxtiff-2.01.eng.tar.gz .

# unpack the fonts, a new english (eng) directory is created with tif/box files
tar -xzf boxtiff-2.01.eng.tar.gz
echo "box/tiff file unpacked"


# Move the arial font data into the build space (yes, the exp0 is required)
mv $TROOT/stockfonts/eng/eng.arial.g4.tif $TROOT/build/eng.arial.exp0.tif
mv $TROOT/stockfonts/eng/eng.arial.box $TROOT/build/eng.arial.exp0.box

mv $TROOT/stockfonts/eng/eng.verdanab.box $TROOT/build/eng.verdanab.box
mv $TROOT/stockfonts/eng/eng.verdanab.g4.tif $TROOT/build/eng.verdanab.exp0.tif

echo "ariel box/tif moved and renamed"
cd $TROOT/build

# Create the font_properties file
echo "arial 0 0 0 0 0" > font_properties
echo "verdanab 0 0 0 0 0" >> font_properties

# BEGIN BUILDING NEW eng.traineddata
tesseract eng.arial.exp0.tif eng.arial.exp0 nobatch box.train
# tesseract eng.verdanab.exp0.tif eng.verdanab.exp0 nobatch box.train

unicharset_extractor eng.arial.exp0.box
# unicharset_extractor eng.verdanab.exp0.box

shapeclustering -F font_properties -U unicharset  eng.arial.exp0.tr
# shapeclustering -F font_properties -U unicharset  eng.verdanab.exp0.tr

mftraining -F font_properties -U unicharset -O TMP_arialb.eng.unicharset eng.arial.exp0.tr
# mftraining -F font_properties -U unicharset -O TMP_verdanaB_eng.unicharset eng.verdanab.exp0.tr

mftraining -F font_properties -U unicharset -O eng.unicharset eng.arial.exp0.tr
# mftraining -F font_properties -U unicharset -O eng.unicharset eng.verdanab.exp0.tr

cntraining eng.arial.exp0.tr
echo "eng.traineddata complete"

# BEGIN combining into an eng.traineddata set
# Note files are moved into an isoloated directory for combiing
# Note files have language prefix added

cp eng.unicharset $TROOT/build/eng/eng.unicharset
cp normproto $TROOT/build/eng/eng.normproto
cp inttemp $TROOT/build/eng/eng.inttemp
cp pffmtable $TROOT/build/eng/eng.pffmtable
cp shapetable $TROOT/build/eng/eng.shapetable

cd $TROOT/build/eng
combine_tessdata eng.

# You now have an eng.trainedddata file in your $TROOT/build/eng directory
# You must move this file to your /usr/local/share/tessdata directory.
# You will need sudo BE.
# permission SURE to back up your old eng.traineddata FIRST
# Recommend testing your new tesseract with the eng.arial.exp0.tif file in
# the build directory.
