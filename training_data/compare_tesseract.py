from Levenshtein import distance
import sys
import os
import pdb
import time

training_text_file = "standard-training-text.txt"

def main(test_file, lang1, lang2):
    print lang1
    print lang2

    s1 = time.time()
    os.system("tesseract {0} out1 -l {1}".format(test_file, lang1))
    e1 = time.time()
    s2 = time.time()
    os.system("tesseract {0} out2 -l {1}".format(test_file, lang2))
    e2 = time.time()

    res1 = open("out1.txt").read()
    res2 = open("out2.txt").read()

    training_text = open(training_text_file).read()

    d1 = distance(training_text_file, res1)
    d2 = distance(training_text_file, res2)

    print "Time for {0}: {1}s".format(lang1, e1 - s1)
    print "Time for {0}: {1}s".format(lang2, e2 - s2)
    print "D1: {0}".format(d1)
    print "D2: {0}".format(d2)
    print "Score: {0}".format(d2/float(d1))


    return

if __name__ == '__main__':
    test_filename = sys.argv[1]
    lang1 = sys.argv[2]
    lang2 = sys.argv[3]
    main(test_filename, lang1, lang2)
